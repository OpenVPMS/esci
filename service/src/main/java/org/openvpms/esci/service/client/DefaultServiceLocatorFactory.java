/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.service.client;

import org.apache.commons.lang.ObjectUtils;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;


/**
 * Default implementation of the {@link ServiceLocatorFactory} interface.
 * <p/>
 * This caches locators.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
public class DefaultServiceLocatorFactory implements ServiceLocatorFactory {

    /**
     * The service authenticators.
     */
    private ServiceAuthenticators authenticators;

    /**
     * Cache of locators.
     */
    private Map<Service, ServiceLocator<?>> locators = new HashMap<Service, ServiceLocator<?>>();

    /**
     * Sets the service authenticators.
     * <p/>
     * This is only used when accessing WSDL that is password protected.
     *
     * @param authenticators the authenticators. May be <tt>null</tt>
     */
    public void setServiceAuthenticators(ServiceAuthenticators authenticators) {
        this.authenticators = authenticators;
    }

    /**
     * Returns a <tt>ServiceLocator</tt> for the specified service interface, WSDL document URL and credentials.
     *
     * @param serviceInterface the service interface
     * @param wsdlDocumentURL  the WSDL document URL
     * @param username         the user name to connect with. May be <tt>null</tt>
     * @param password         the password to connect with. May be <tt>null</tt>
     * @return a service locator
     * @throws java.net.MalformedURLException if the WSDL document URL is invalid
     */
    @Override
    public <T> ServiceLocator<T> getServiceLocator(Class<T> serviceInterface, String wsdlDocumentURL, String username,
                                                   String password) throws MalformedURLException {
        return getServiceLocator(serviceInterface, wsdlDocumentURL, null, username, password);
    }

    /**
     * Returns a <tt>ServiceLocator</tt> for the specified service interface, WSDL document URL and credentials.
     *
     * @param serviceInterface the service interface
     * @param wsdlDocumentURL  the WSDL document URL
     * @param endpointAddress  the endpoint address. May be <tt>null</tt>
     * @param username         the user name to connect with. May be <tt>null</tt>
     * @param password         the password to connect with. May be <tt>null</tt>
     * @return a service locator
     * @throws java.net.MalformedURLException if the WSDL document URL is invalid
     */
    @SuppressWarnings("unchecked")
    @Override
    public <T> ServiceLocator<T> getServiceLocator(Class<T> serviceInterface, String wsdlDocumentURL,
                                                   String endpointAddress, String username,
                                                   String password) throws MalformedURLException {
        Service service = new Service(serviceInterface, wsdlDocumentURL, endpointAddress, username, password);
        DefaultServiceLocator<T> locator = (DefaultServiceLocator<T>) locators.get(service);
        if (locator == null) {
            locator = DefaultServiceLocator.create(serviceInterface, wsdlDocumentURL, endpointAddress);
            locator.setServiceAuthenticators(authenticators);
            locator.setUsername(username);
            locator.setPassword(password);
            locators.put(service, locator);
        }

        return locator;
    }

    /**
     * Helper to cache locators.
     */
    static class Service {

        /**
         * The service interface.
         */
        private final Class serviceInterface;

        /**
         * The WSDL document URL.
         */
        private final String wsdlDocumentURL;

        /**
         * The endpoint address.
         */
        private final String endpointAddress;

        /**
         * The username for authentication.
         */
        private final String username;

        /**
         * The password for authentication.
         */
        private final String password;

        public Service(Class serviceInterface, String wsdlDocumentURL, String endpointAddress,
                       String username, String password) {
            this.serviceInterface = serviceInterface;
            this.wsdlDocumentURL = wsdlDocumentURL;
            this.endpointAddress = endpointAddress;
            this.username = username;
            this.password = password;
        }

        /**
         * Returns a hash code value for the object.
         */
        @Override
        public int hashCode() {
            return wsdlDocumentURL.hashCode() & serviceInterface.hashCode();
        }

        /**
         * Indicates whether some other object is "equal to" this one.
         *
         * @param obj the reference object with which to compare.
         * @return <tt>true</tt> if this object is the same as the obj argument; <tt>false</tt> otherwise.
         */
        @Override
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj instanceof Service) {
                Service other = (Service) obj;
                return ObjectUtils.equals(serviceInterface, other.serviceInterface)
                       && ObjectUtils.equals(wsdlDocumentURL, other.wsdlDocumentURL)
                       && ObjectUtils.equals(endpointAddress, other.endpointAddress)
                       && ObjectUtils.equals(username, other.username)
                       && ObjectUtils.equals(password, other.password);
            }
            return false;
        }
    }
}
