/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.service;

import org.openvpms.esci.service.exception.DuplicateOrderException;
import org.openvpms.esci.ubl.order.Order;

import javax.annotation.Resource;
import javax.jws.WebService;


/**
 * Default implementation of the {@link OrderService}.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
@WebService(endpointInterface = "org.openvpms.esci.service.OrderService",
            targetNamespace = "http://openvpms.org/esci", serviceName = "OrderService",
            portName = "OrderServicePort")
public class OrderWebService implements OrderService {

    /**
     * The order service to delegate to.
     */
    private OrderService service;


    /**
     * Submits an order.
     *
     * @param order the order to submit
     * @throws DuplicateOrderException if the order has a duplicate identifier to one already submitted
     */
    public void submitOrder(Order order) throws DuplicateOrderException {
        getOrderService().submitOrder(order);
    }

    /**
     * Registers the order service implementation.
     *
     * @param service the order service
     */
    @Resource
    public void setOrderService(OrderService service) {
        this.service = service;
    }

    /**
     * Returns the order service.
     *
     * @return the order service
     */
    public OrderService getOrderService() {
        return service;
    }

}
