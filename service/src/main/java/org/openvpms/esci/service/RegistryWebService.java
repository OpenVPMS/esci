/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2011 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id: $
 */

package org.openvpms.esci.service;

import javax.annotation.Resource;
import javax.jws.WebService;


/**
 * Default implementation of the {@link RegistryService}.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: $
 */
@WebService(endpointInterface = "org.openvpms.esci.service.RegistryService",
            targetNamespace = "http://openvpms.org/esci", serviceName = "RegistryService",
            portName = "RegistryServicePort")
public class RegistryWebService implements RegistryService {

    /**
     * The service to delegate to.
     */
    private RegistryService service;

    /**
     * Returns the InboxService WSDL URL.
     *
     * @return the InboxService WSDL URL
     */
    @Override
    public String getInboxService() {
        return service.getInboxService();
    }

    /**
     * Returns the OrderService WSDL URL.
     *
     * @return the OrderService WSDL URL
     */
    @Override
    public String getOrderService() {
        return service.getOrderService();
    }

    /**
     * Registers the registry service to delegate to.
     *
     * @param service the registry service
     */
    @Resource
    public void setRegistry(RegistryService service) {
        this.service = service;
    }

    /**
     * Returns the registry service.
     *
     * @return the registry service
     */
    public RegistryService getRegistry() {
        return service;
    }
}
