/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.service;

import org.openvpms.esci.service.exception.DocumentNotFoundException;
import org.openvpms.esci.ubl.common.Document;
import org.openvpms.esci.ubl.common.aggregate.DocumentReferenceType;

import java.util.List;


/**
 * Implementation of the {@link InboxService} that delegates to another.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
public class DelegatingInboxService implements InboxService {

    /**
     * The service to delegate to.
     */
    private InboxService service;

    /**
     * Returns a list of document references, in the order that they were received.
     * <p/>
     * Each reference uniquely identifies a single document.
     *
     * @return a list of document references
     */
    @Override
    public List<DocumentReferenceType> getDocuments() {
        return service.getDocuments();
    }

    /**
     * Returns the document with the specified reference.
     *
     * @param reference the document reference
     * @return the corresponding document, or <tt>null</tt> if the document is not found
     */
    @Override
    public Document getDocument(DocumentReferenceType reference) {
        return service.getDocument(reference);
    }

    /**
     * Acknowledges a document.
     * <p/>
     * Once acknowledged, the document will no longer be returned by {@link #getDocuments} nor {@link #getDocument}.
     *
     * @param reference the document reference
     * @throws DocumentNotFoundException if the reference doesn't refer to a valid document
     */
    @Override
    public void acknowledge(DocumentReferenceType reference) throws DocumentNotFoundException {
        service.acknowledge(reference);
    }

    /**
     * Registers the service to delegate to.
     *
     * @param service the service
     */
    public void setInboxService(InboxService service) {
        this.service = service;
    }

}


