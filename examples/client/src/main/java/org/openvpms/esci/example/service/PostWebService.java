/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2011 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id: $
 */

package org.openvpms.esci.example.service;

import javax.annotation.Resource;
import javax.jws.WebService;

/**
 * Default implementation of the {@link PostService}.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: $
 */
@WebService(endpointInterface = "org.openvpms.esci.example.service.PostService",
            targetNamespace = "http://openvpms.org/esci/example", serviceName = "PostService",
            portName = "PostServicePort")
public class PostWebService implements PostService {

    /**
     * The service to delegate to.
     */
    private PostService service;

    /**
     * Post a document to the inbox service.
     *
     * @param document the document to post
     */
    @Override
    public void post(Object document) {
        service.post(document);
    }

    /**
     * Registers the service to delegate to.
     *
     * @param service the service
     */
    @Resource
    public void setPostService(PostService service) {
        this.service = service;
    }
}
