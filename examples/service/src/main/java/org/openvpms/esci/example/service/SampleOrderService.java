/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.example.service;

import org.openvpms.esci.service.OrderService;
import org.openvpms.esci.service.exception.DuplicateOrderException;
import org.openvpms.esci.ubl.common.AmountType;
import org.openvpms.esci.ubl.common.CurrencyCodeContentType;
import org.openvpms.esci.ubl.common.IdentifierType;
import org.openvpms.esci.ubl.common.QuantityType;
import org.openvpms.esci.ubl.common.aggregate.DocumentReferenceType;
import org.openvpms.esci.ubl.common.aggregate.InvoiceLineType;
import org.openvpms.esci.ubl.common.aggregate.MonetaryTotalType;
import org.openvpms.esci.ubl.common.aggregate.OrderLineReferenceType;
import org.openvpms.esci.ubl.common.aggregate.OrderLineType;
import org.openvpms.esci.ubl.common.aggregate.OrderReferenceType;
import org.openvpms.esci.ubl.common.aggregate.PriceType;
import org.openvpms.esci.ubl.common.aggregate.TaxCategoryType;
import org.openvpms.esci.ubl.common.aggregate.TaxSchemeType;
import org.openvpms.esci.ubl.common.aggregate.TaxSubtotalType;
import org.openvpms.esci.ubl.common.aggregate.TaxTotalType;
import org.openvpms.esci.ubl.common.basic.AcceptedIndicatorType;
import org.openvpms.esci.ubl.common.basic.IDType;
import org.openvpms.esci.ubl.common.basic.InvoicedQuantityType;
import org.openvpms.esci.ubl.common.basic.IssueDateType;
import org.openvpms.esci.ubl.common.basic.LineExtensionAmountType;
import org.openvpms.esci.ubl.common.basic.LineIDType;
import org.openvpms.esci.ubl.common.basic.PayableAmountType;
import org.openvpms.esci.ubl.common.basic.PercentType;
import org.openvpms.esci.ubl.common.basic.PriceAmountType;
import org.openvpms.esci.ubl.common.basic.TaxAmountType;
import org.openvpms.esci.ubl.common.basic.TaxExclusiveAmountType;
import org.openvpms.esci.ubl.common.basic.TaxTypeCodeType;
import org.openvpms.esci.ubl.common.basic.UBLVersionIDType;
import org.openvpms.esci.ubl.invoice.Invoice;
import org.openvpms.esci.ubl.invoice.InvoiceType;
import org.openvpms.esci.ubl.order.Order;
import org.openvpms.esci.ubl.order.OrderResponseSimple;
import org.openvpms.esci.ubl.order.OrderResponseSimpleType;
import org.openvpms.esci.ubl.order.OrderType;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;


/**
 * Sample order service.
 * <p/>
 * This creates an OrderResponseSimple and Invoice document for each received order.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
public class SampleOrderService extends SampleService implements OrderService {

    /**
     * Response id seed.
     */
    private int responseIdSeed = 1;

    /**
     * Invoice id seed.
     */
    private int invoiceIdSeed = 1;

    /**
     * The inbox service.
     */
    private SampleInboxService inbox;

    /**
     * Data type factory.
     */
    private DatatypeFactory factory;

    /**
     * The set of order identifiers already received.
     */
    private Set<String> orders = Collections.synchronizedSet(new HashSet<String>());

    /**
     * Determines if duplicate orders should be ignored.
     */
    private boolean ignoreDuplicates;

    /**
     * GST rate.
     */
    private static final BigDecimal GST = new BigDecimal("0.10");


    /**
     * Default constructor.
     *
     * @throws DatatypeConfigurationException if the xml data factory cannot be created
     * @throws JAXBException                  if the document context cannot be created
     * @throws SAXException                   if the document context cannot be created
     */
    public SampleOrderService() throws DatatypeConfigurationException, JAXBException, SAXException {
        factory = DatatypeFactory.newInstance();
    }

    /**
     * Determines if duplicate orders should be ignored.
     * <p/>
     * Defaults to <tt>false</tt>.
     *
     * @param ignoreDuplicates if <tt>true</tt> ignore duplicates
     */
    public void setIgnoreDuplicates(boolean ignoreDuplicates) {
        this.ignoreDuplicates = ignoreDuplicates;
    }

    /**
     * Sets the inbox.
     *
     * @param inbox the inbox
     */
    public void setInbox(SampleInboxService inbox) {
        this.inbox = inbox;
    }

    /**
     * Submits an order.
     *
     * @param order the order to submit
     * @throws DuplicateOrderException if the order has a duplicate identifier to one already submitted
     */
    @Override
    public void submitOrder(Order order) throws DuplicateOrderException {
        log(order);
        if (!ignoreDuplicates && orders.contains(order.getID().getValue())) {
            throw new DuplicateOrderException("Duplicate order: " + order.getID().getValue());
        }
        orders.add(order.getID().getValue());

        OrderResponseSimpleType response = createResponse(order);
        DocumentReferenceType responseRef = createDocumentReference("OrderResponseSimple", response.getID());
        inbox.addDocument(responseRef, response);

        InvoiceType invoice = createInvoice(order);
        DocumentReferenceType invoiceRef = createDocumentReference("Invoice", invoice.getID());
        inbox.addDocument(invoiceRef, invoice);
    }

    /**
     * Creates a response to an order.
     *
     * @param order the order
     * @return the response
     */
    private OrderResponseSimpleType createResponse(OrderType order) {
        OrderResponseSimple response = new OrderResponseSimple();
        response.setID(createID(responseIdSeed++));
        response.setUBLVersionID(getID(new UBLVersionIDType(), "2.0"));
        response.setIssueDate(createIssueDate(new Date()));
        AcceptedIndicatorType accepted = new AcceptedIndicatorType();
        accepted.setValue(true);
        response.setAcceptedIndicator(accepted);
        OrderReferenceType ref = new OrderReferenceType();
        ref.setID(order.getID());
        response.setOrderReference(ref);
        response.setBuyerCustomerParty(order.getBuyerCustomerParty());
        response.setSellerSupplierParty(order.getSellerSupplierParty());
        return response;
    }

    /**
     * Creates an invoice for an order.
     *
     * @param order the order
     * @return the corresponding invoice
     */
    private InvoiceType createInvoice(OrderType order) {
        long itemId = 1;
        Invoice invoice = new Invoice();
        invoice.setID(createID(invoiceIdSeed++));
        invoice.setUBLVersionID(getID(new UBLVersionIDType(), "2.0"));
        Date now = new Date();
        invoice.setIssueDate(createIssueDate(now));
        invoice.setOrderReference(createOrderReference(order));
        invoice.setAccountingSupplierParty(order.getSellerSupplierParty());
        invoice.setAccountingCustomerParty(order.getBuyerCustomerParty());
        BigDecimal tax = BigDecimal.ZERO;
        CurrencyCodeContentType ccy = order.getAnticipatedMonetaryTotal().getPayableAmount().getCurrencyID();
        BigDecimal lineExtensionAmount = BigDecimal.ZERO;
        BigDecimal taxExclusiveAmount = BigDecimal.ZERO;
        for (OrderLineType orderLine : order.getOrderLine()) {
            InvoiceLineType invoiceLine = new InvoiceLineType();
            PriceType price = orderLine.getLineItem().getPrice();
            PriceAmountType priceAmount = price.getPriceAmount();
            QuantityType quantity = orderLine.getLineItem().getQuantity();
            BigDecimal amount = round(priceAmount.getValue().multiply(quantity.getValue()));
            lineExtensionAmount = lineExtensionAmount.add(amount);
            taxExclusiveAmount = taxExclusiveAmount.add(amount);
            TaxTotalType taxTotal = createTaxTotal(amount, ccy);
            invoiceLine.getTaxTotal().add(taxTotal);
            tax = tax.add(taxTotal.getTaxAmount().getValue());
            invoiceLine.setID(createID(itemId++));
            invoiceLine.getOrderLineReference().add(createOrderLineReference(orderLine));
            invoiceLine.setLineExtensionAmount(getAmount(new LineExtensionAmountType(), ccy, amount));
            invoiceLine.setInvoicedQuantity(getQuantity(new InvoicedQuantityType(), quantity));
            invoiceLine.setItem(orderLine.getLineItem().getItem());
            invoiceLine.setPrice(price);

            invoice.getInvoiceLine().add(invoiceLine);
        }
        MonetaryTotalType legalMonetaryTotal = new MonetaryTotalType();
        BigDecimal payableAmount = lineExtensionAmount.add(tax);
        legalMonetaryTotal.setLineExtensionAmount(getAmount(new LineExtensionAmountType(), ccy, lineExtensionAmount));
        legalMonetaryTotal.setTaxExclusiveAmount(getAmount(new TaxExclusiveAmountType(), ccy, lineExtensionAmount));
        legalMonetaryTotal.setPayableAmount(getAmount(new PayableAmountType(), ccy, payableAmount));
        invoice.setLegalMonetaryTotal(legalMonetaryTotal);
        TaxTotalType taxTotal = new TaxTotalType();
        taxTotal.setTaxAmount(getAmount(new TaxAmountType(), ccy, tax));
        invoice.getTaxTotal().add(taxTotal);
        return invoice;
    }

    /**
     * Creates an issue date.
     *
     * @param date the date
     * @return a new issue date
     */
    private IssueDateType createIssueDate(Date date) {
        IssueDateType result = new IssueDateType();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        XMLGregorianCalendar xml = factory.newXMLGregorianCalendar(calendar);
        xml.setHour(DatatypeConstants.FIELD_UNDEFINED);
        xml.setMinute(DatatypeConstants.FIELD_UNDEFINED);
        xml.setSecond(DatatypeConstants.FIELD_UNDEFINED);
        xml.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
        xml.setTimezone(DatatypeConstants.FIELD_UNDEFINED);
        result.setValue(xml);
        return result;
    }

    /**
     * Creates an order reference.
     *
     * @param order the order
     * @return a reference to the order
     */
    private OrderReferenceType createOrderReference(OrderType order) {
        OrderReferenceType result = new OrderReferenceType();
        result.setID(order.getID());
        return result;
    }

    /**
     * Creates an order line reference.
     *
     * @param orderLine the order line
     * @return a reference to the order line
     */
    private OrderLineReferenceType createOrderLineReference(OrderLineType orderLine) {
        OrderLineReferenceType result = new OrderLineReferenceType();
        LineIDType lineId = new LineIDType();
        lineId.setValue(orderLine.getLineItem().getID().getValue());
        result.setLineID(lineId);
        return result;
    }

    /**
     * Creates a tax total.
     *
     * @param amount   the amount to calculate tax for
     * @param currency the currency
     * @return a new tax total
     */
    private TaxTotalType createTaxTotal(BigDecimal amount, CurrencyCodeContentType currency) {
        TaxTotalType result = new TaxTotalType();
        BigDecimal tax = round(amount.multiply(GST));
        result.setTaxAmount(getAmount(new TaxAmountType(), currency, tax));
        TaxSubtotalType subtotal = new TaxSubtotalType();
        subtotal.setTaxAmount(result.getTaxAmount());
        subtotal.setTaxCategory(createTaxCategory());
        result.getTaxSubtotal().add(subtotal);
        return result;
    }

    /**
     * Helper to populate an amount.
     *
     * @param amount   the amount to populate
     * @param currency the currency code
     * @param value    the amount value
     * @return the populated amount
     */
    private <T extends AmountType> T getAmount(T amount, CurrencyCodeContentType currency, BigDecimal value) {
        amount.setCurrencyID(currency);
        amount.setValue(value);
        return amount;
    }

    /**
     * Rounds a value to 2 decimal places.
     *
     * @param value the value to round
     * @return the rounded value
     */
    private BigDecimal round(BigDecimal value) {
        return value.setScale(2, RoundingMode.HALF_UP);
    }

    /**
     * Helper to populate a quantity.
     *
     * @param quantity the quantity to populate
     * @param source   the quantity to populate from
     * @return the populated quantity
     */
    private <T extends QuantityType> T getQuantity(T quantity, QuantityType source) {
        quantity.setValue(source.getValue());
        quantity.setUnitCode(source.getUnitCode());
        return quantity;
    }

    /**
     * Creates a new 'standard rated' GST tax category.
     *
     * @return a new tax category
     */
    private TaxCategoryType createTaxCategory() {
        TaxCategoryType result = new TaxCategoryType();
        result.setID(createID("S"));
        PercentType pc = new PercentType();
        pc.setValue(new BigDecimal("10.00"));
        result.setPercent(pc);
        TaxSchemeType scheme = new TaxSchemeType();
        TaxTypeCodeType taxType = new TaxTypeCodeType();
        taxType.setValue("GST");
        scheme.setTaxTypeCode(taxType);
        result.setTaxScheme(scheme);
        return result;
    }

    /**
     * Helper to create an <tt>IDType</tt>.
     *
     * @param value the id value
     * @return a new <tt>IDType</tt>
     */
    private IDType createID(long value) {
        return createID(Long.toHexString(value));
    }

    /**
     * Helper to create an <tt>IDType</tt>.
     *
     * @param value the id value
     * @return a new <tt>IDType</tt>
     */
    private IDType createID(String value) {
        IDType result = new IDType();
        result.setValue(value);
        return result;
    }

    /**
     * Helper to populate an identifier.
     *
     * @param id    the identifier to populate
     * @param value the value to populate with
     * @return the populated identifier
     */
    private <T extends IdentifierType> T getID(T id, String value) {
        id.setValue(value);
        return id;
    }

}
