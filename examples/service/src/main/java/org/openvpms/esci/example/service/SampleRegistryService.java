/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.example.service;

import org.openvpms.esci.service.RegistryService;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;


/**
 * Sample registry service.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
@WebService(endpointInterface = "org.openvpms.esci.service.RegistryService",
            targetNamespace = "http://openvpms.org/esci", serviceName = "RegistryService",
            portName = "RegistryServicePort")
public class SampleRegistryService implements RegistryService {

    /**
     * The web services context.
     */
    @Resource
    private WebServiceContext serviceContext;

    /**
     * Returns the InboxService WSDL URL.
     *
     * @return the InboxService WSDL URL
     */
    public String getInboxService() {
        return getURL("InboxService");
    }

    /**
     * Returns the OrderService WSDL URL.
     *
     * @return the OrderService WSDL URL
     */
    public String getOrderService() {
        return getURL("OrderService");
    }

    /**
     * Returns a service URI.
     *
     * @param service the service
     * @return the service WSDL URL
     */
    private String getURL(String service) {
        MessageContext context = serviceContext.getMessageContext();
        HttpServletRequest request = (HttpServletRequest) context.get(MessageContext.SERVLET_REQUEST);
        String url = request.getRequestURL().toString();
        url = url.substring(0, url.lastIndexOf('/') + 1) + service + "?wsdl";
        return url;
    }
}