/*
 * Version: 1.0
 *
 * The contents of this file are subject to the OpenVPMS License Version
 * 1.0 (the 'License'); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.openvpms.org/license/
 *
 * Software distributed under the License is distributed on an 'AS IS' basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyright 2020 (C) OpenVPMS Ltd. All Rights Reserved.
 */
package org.openvpms.esci.example.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Set;


/**
 * A {@code SOAPHandler} that logs messages.
 *
 * @author Tim Anderson
 */
public class LoggingSOAPHandler implements SOAPHandler<SOAPMessageContext> {

    /**
     * The log.
     */
    private static final Logger log = LoggerFactory.getLogger(LoggingSOAPHandler.class);

    /**
     * Gets the header blocks that can be processed by this Handler
     * instance.
     * <p/>
     * This implementation always returns {@code null}
     *
     * @return {@code null}
     */
    public Set<QName> getHeaders() {
        return null;
    }

    /**
     * Logs a message.
     *
     * @param context the soap message context
     * @return {@code true}
     */
    public boolean handleMessage(final SOAPMessageContext context) {
        if (log.isDebugEnabled() || log.isInfoEnabled()) {
            String message;
            if (log.isDebugEnabled()) {
                message = buildDebugMessage(context);
            } else {
                message = buildInfoMessage(context);
            }
            log.info(message);
        }

        return true;
    }

    /**
     * Logs a fault.
     *
     * @param context the message context
     * @return true always
     */
    public boolean handleFault(final SOAPMessageContext context) {
        if (log.isWarnEnabled()) {
            final String message = buildDebugMessage(context);
            String fault = null;
            try {
                fault = context.getMessage().getSOAPBody().getFault().getFaultString();
            } catch (SOAPException exception) {
                log.error(String.format("Unable to evaluate SOAPFault for message %s", message), exception);
            }
            log.warn(String.format("%s,fault=%s", message, fault));
        }

        return true;
    }

    /**
     * Logs a close.
     *
     * @param context the message context
     */
    public void close(MessageContext context) {
        if (log.isDebugEnabled()) {
            final Boolean outbound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
            log.debug(String.format("Closed:direction=%s,service=%s,operationName=%s", (outbound ? "outbound" : "inbound"), context.get(MessageContext.WSDL_SERVICE), context.get(MessageContext.WSDL_OPERATION)));
        }
    }

    private String buildDebugMessage(final SOAPMessageContext context) {
        final Boolean outbound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

        String message;
        Throwable throwable = null;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            context.getMessage().writeTo(out);
        } catch (SOAPException exception) {
            throwable = exception;
        } catch (IOException ex) {
            throwable = ex;
        }
        if (throwable == null) {
            message = String.format("Handled:direction=%s,service=%s,operationName=%s,message=%s", outbound ? "outbound" : "inbound", context.get(MessageContext.WSDL_SERVICE), context.get(MessageContext.WSDL_OPERATION), out);
        } else {
            message = String.format("Handled:direction=%s,service=%s,operationName=%s,exception=%s", outbound ? "outbound" : "inbound", context.get(MessageContext.WSDL_SERVICE), context.get(MessageContext.WSDL_OPERATION), throwable);
        }
        return message;
    }

    private String buildInfoMessage(final SOAPMessageContext context) {
        String message;
        final Boolean outbound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        message =
                String.format("Handled:direction=%s,service=%s,operationName=%s", outbound ? "outbound" : "inbound", context.get(MessageContext.WSDL_SERVICE), context.get(MessageContext.WSDL_OPERATION));
        return message;
    }
}