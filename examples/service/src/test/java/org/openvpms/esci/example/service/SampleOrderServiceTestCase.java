/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2011 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id: $
 */

package org.openvpms.esci.example.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.openvpms.esci.ubl.common.Document;
import org.openvpms.esci.ubl.common.aggregate.DocumentReferenceType;
import org.openvpms.esci.ubl.common.aggregate.OrderLineType;
import org.openvpms.esci.ubl.common.aggregate.InvoiceLineType;
import org.openvpms.esci.ubl.invoice.InvoiceType;
import org.openvpms.esci.ubl.io.UBLDocumentContext;
import org.openvpms.esci.ubl.order.Order;
import org.openvpms.esci.ubl.order.OrderResponseSimpleType;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;


/**
 * Tests the {@link SampleOrderService} class.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: $
 */
public class SampleOrderServiceTestCase {

    /**
     * Document context.
     */
    private UBLDocumentContext context;

    /**
     * Sets up the test case.
     *
     * @throws Exception for any error
     */
    @Before
    public void setUp() throws Exception {
        context = new UBLDocumentContext();
    }

    /**
     * Verifies that an order can be submitted, and that a response and invoice are generated in the Inbox.
     *
     * @throws Exception for any error
     */
    @Test
    public void testSubmitOrder() throws Exception {
        SampleOrderService service = new SampleOrderService();
        SampleInboxService inbox = new SampleInboxService();
        service.setInbox(inbox);

        // read a valid order from a file
        InputStream stream = getClass().getResourceAsStream("/order.xml");
        assertNotNull(stream);
        Order order = (Order) context.createReader().read(stream);
        service.submitOrder(order);

        // verify there are 2 documents in the inbox
        List<DocumentReferenceType> references = inbox.getDocuments();
        assertEquals(2, references.size());

        // verify that the first document is an order response
        DocumentReferenceType responseRef = references.get(0);
        checkValid(responseRef);
        assertEquals("OrderResponseSimple", responseRef.getDocumentType().getValue());
        Document doc = inbox.getDocument(responseRef);
        assertNotNull(doc);
        OrderResponseSimpleType response = (OrderResponseSimpleType) doc.getAny();
        assertNotNull(response);
        checkValid(response);

        // verify that the second document is an invoice
        DocumentReferenceType invoiceRef = references.get(1);
        checkValid(invoiceRef);
        assertEquals("Invoice", invoiceRef.getDocumentType().getValue());
        doc = inbox.getDocument(invoiceRef);
        assertNotNull(doc);
        InvoiceType invoice = (InvoiceType) doc.getAny();
        assertNotNull(invoice);
        checkValid(invoice);

        // verify the response corresponds to the original order, and is accepted
        assertEquals(order.getID().getValue(), response.getOrderReference().getID().getValue());
        assertTrue(response.getAcceptedIndicator().isValue());

        // verify the invoice corresponds to the order
        assertEquals(order.getID().getValue(), invoice.getOrderReference().getID().getValue());
        assertEquals(order.getTaxTotal().get(0).getTaxAmount().getValue(),
                     invoice.getTaxTotal().get(0).getTaxAmount().getValue());
        assertEquals(order.getAnticipatedMonetaryTotal().getLineExtensionAmount().getValue(),
                     invoice.getLegalMonetaryTotal().getLineExtensionAmount().getValue());
        assertEquals(order.getAnticipatedMonetaryTotal().getPayableAmount().getValue(),
                     invoice.getLegalMonetaryTotal().getPayableAmount().getValue());

        // verify the invoice lines correspond to the order lines
        assertEquals(order.getOrderLine().size(), invoice.getInvoiceLine().size());
        for (int i = 0; i < order.getOrderLine().size(); ++i) {
            OrderLineType orderItem = order.getOrderLine().get(i);
            InvoiceLineType invoiceItem = invoice.getInvoiceLine().get(i);
            assertEquals(orderItem.getLineItem().getTotalTaxAmount().getValue(),
                         invoiceItem.getTaxTotal().get(0).getTaxAmount().getValue());

            assertEquals(orderItem.getLineItem().getLineExtensionAmount().getValue(),
                        invoiceItem.getLineExtensionAmount().getValue());
        }
    }

    /**
     * Validates a document.
     *
     * @param document the document to validate
     * @throws JAXBException if the document is invalid
     */
    private void checkValid(Object document) throws JAXBException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        context.createWriter().write(document, stream, true);
    }

}
