/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.ubl.io;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;


/**
 * UBL Document reader.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
public class UBLDocumentReader {

    /**
     * The document context.
     */
    private final UBLDocumentContext context;

    
    /**
     * Constructs an <tt>UBLDocumentReader</tt>.
     *
     * @param context the document context
     */
    public UBLDocumentReader(UBLDocumentContext context) {
        this.context = context;
    }

    /**
     * Reads an UBL document from a file.
     * <p/>
     * Validation will be performed.
     *
     * @param file the file to read from
     * @return a instance of the JAXB class representing the document
     * @throws FileNotFoundException if the file cannot be found
     * @throws JAXBException         if the document cannot be read
     */
    public Object read(File file) throws FileNotFoundException, JAXBException {
        return read(new FileInputStream(file));
    }

    /**
     * Reads an UBL document from a stream.
     * <p/>
     * Validation will be performed.
     *
     * @param stream the stream to read from
     * @return a instance of the JAXB class representing the document
     * @throws JAXBException if the document cannot be read
     */
    public Object read(InputStream stream) throws JAXBException {
        return read(stream, true);
    }

    /**
     * Reads an UBL document from a stream, optionally performing validation.
     *
     * @param stream   the stream to read from
     * @param validate if <tt>true</tt> validate the document
     * @return a instance of the JAXB class representing the document
     * @throws JAXBException if the document cannot be read
     */
    public Object read(InputStream stream, boolean validate) throws JAXBException {
        Unmarshaller unmarshaller = context.getContext().createUnmarshaller();
        if (validate) {
            unmarshaller.setSchema(context.getSchema());
        }
        Object result = unmarshaller.unmarshal(stream);
        if (result instanceof JAXBElement) {
            result = ((JAXBElement) result).getValue();
        }
        return result;
    }

}
