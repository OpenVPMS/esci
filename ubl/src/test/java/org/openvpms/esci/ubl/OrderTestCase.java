/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.ubl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.openvpms.esci.ubl.common.aggregate.CustomerPartyType;
import org.openvpms.esci.ubl.common.aggregate.LineItemType;
import org.openvpms.esci.ubl.common.aggregate.OrderLineType;
import org.openvpms.esci.ubl.common.aggregate.SupplierPartyType;
import org.openvpms.esci.ubl.common.aggregate.TransactionConditionsType;
import org.openvpms.esci.ubl.common.basic.PayableAmountType;
import org.openvpms.esci.ubl.io.UBLDocumentContext;
import org.openvpms.esci.ubl.order.OrderType;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;


/**
 * Tests unmarshalling and marshalling of orders.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
public class OrderTestCase extends MarshallingTestCase {

    /**
     * Unmarshals an order from a file, marshals it to memory, and unmarshals again.
     * The final unmarshalled object is verified against the expected results.
     *
     * @throws Exception for any error
     */
    @Test
    public void testRoundtrip() throws Exception {
        UBLDocumentContext context = new UBLDocumentContext();

        // read a valid order from a file
        InputStream stream = OrderTestCase.class.getResourceAsStream("/order.xml");
        assertNotNull(stream);
        OrderType order = (OrderType) context.createReader().read(stream);

        // write it to a byte stream
        ByteArrayOutputStream ostream = new ByteArrayOutputStream();
        context.createWriter().write(order, ostream);

        // read the order from the byte stream
        ByteArrayInputStream istream = new ByteArrayInputStream(ostream.toByteArray());
        order = (OrderType) context.createReader().read(istream);

        // now verify the order has the expected content
        checkId("2.0", order.getUBLVersionID());
        assertEquals(false, order.getCopyIndicator().isValue());
        checkId("6E09886B-DC6E-439F-82D1-7CCAC7F4E3B1", order.getUUID());
        assertEquals("2010-01-14", order.getIssueDate().getValue().toString());
        assertEquals(1, order.getNote().size());
        assertEquals("Sample Order", order.getNote().get(0).getValue());
        CustomerPartyType customer = order.getBuyerCustomerParty();
        assertEquals("CV12345", customer.getSupplierAssignedAccountID().getValue());
        checkParty(customer.getParty(), "Phillip Island Veterinary Clinic", "42", "Phillip Island Rd",
                   "New Haven", "3925", "VIC");

        SupplierPartyType supplier = order.getSellerSupplierParty();
        assertEquals("1234", supplier.getCustomerAssignedAccountID().getValue());
        checkParty(supplier.getParty(), "Cenvet Pty Ltd", "26", "Binney Road", "Kings Park", "2148", "NSW");

        TransactionConditionsType conditions = order.getTransactionConditions();
        assertEquals(1, conditions.getDescription().size());
        assertEquals("Order response required; payment is by BACS or by cheque",
                     conditions.getDescription().get(0).getValue());

        PayableAmountType amount = order.getAnticipatedMonetaryTotal().getPayableAmount();
        checkAmount("170.50", amount);

        assertEquals(2, order.getOrderLine().size());
        OrderLineType line1 = order.getOrderLine().get(0);
        LineItemType item1 = line1.getLineItem();
        assertEquals("Order Line 1", line1.getNote().getValue());
        checkId("20123", item1.getID());
        checkQuantity(item1.getQuantity(), "BO", 2);
        checkAmount("100.00", item1.getLineExtensionAmount());
        checkAmount("10.00", item1.getTotalTaxAmount());
        checkAmount("50.00", item1.getPrice().getPriceAmount());
        checkQuantity(item1.getPrice().getBaseQuantity(), "BO", 1);
        checkItem(item1.getItem(), "Amoxil 200mg Tablets", "Bottle Amoxil 200mg Tablets", "123456", "AMOX200100");

        OrderLineType line2 = order.getOrderLine().get(1);
        LineItemType item2 = line2.getLineItem();
        assertEquals("Order Line 2", line2.getNote().getValue());
        assertEquals("20124", item2.getID().getValue());
        checkQuantity(item2.getQuantity(), "PK", 1);
        checkAmount("55.00", item2.getLineExtensionAmount());
        checkAmount("5.50", item2.getTotalTaxAmount());
        checkAmount("55.00", item2.getPrice().getPriceAmount());
        checkQuantity(item2.getPrice().getBaseQuantity(), "PK", 1);
        checkItem(item2.getItem(), "Advantage Dog Large", "Advantage Dog Large", "123512", "ADVAND623");
    }

}
