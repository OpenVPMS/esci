/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.ubl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.openvpms.esci.ubl.common.aggregate.CustomerPartyType;
import org.openvpms.esci.ubl.common.aggregate.InvoiceLineType;
import org.openvpms.esci.ubl.common.aggregate.MonetaryTotalType;
import org.openvpms.esci.ubl.common.aggregate.OrderLineReferenceType;
import org.openvpms.esci.ubl.common.aggregate.SupplierPartyType;
import org.openvpms.esci.ubl.invoice.InvoiceType;
import org.openvpms.esci.ubl.io.UBLDocumentContext;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;


/**
 * Tests unmarshalling and marshalling of invoices.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
public class InvoiceTestCase extends MarshallingTestCase {

    /**
     * Unmarshals an invoice from a file, marshals it to memory, and unmarshals again.
     * The final unmarshalled object is verified against the expected results.
     *
     * @throws Exception for any error
     */
    @Test
    public void testRoundtrip() throws Exception {
        UBLDocumentContext context = new UBLDocumentContext();

        // read a valid invoice from a file
        InputStream stream = InvoiceTestCase.class.getResourceAsStream("/invoice.xml");
        assertNotNull(stream);
        InvoiceType invoice = (InvoiceType) context.createReader().read(stream);

        // write it to a byte stream
        ByteArrayOutputStream ostream = new ByteArrayOutputStream();
        context.createWriter().write(invoice, ostream);

        // read the invoice from the byte stream
        ByteArrayInputStream istream = new ByteArrayInputStream(ostream.toByteArray());
        invoice = (InvoiceType) context.createReader().read(istream);

        // now verify the invoice has the expected content
        checkId("2.0", invoice.getUBLVersionID());
        assertEquals("2005-06-21", invoice.getIssueDate().getValue().toString());
        assertEquals(1, invoice.getNote().size());
        assertEquals("Sample Invoice", invoice.getNote().get(0).getValue());

        // check the order reference
        checkId("AEG012345", invoice.getOrderReference().getID());
        checkId("CON0095678", invoice.getOrderReference().getSalesOrderID());

        // check the customer
        CustomerPartyType customer = invoice.getAccountingCustomerParty();
        checkId("GT00978567", customer.getSupplierAssignedAccountID());
        checkParty(customer.getParty(), "Phillip Island Veterinary Clinic", "42", "Phillip Island Rd",
                   "New Haven", "3925", "VIC");

        // check the supplier
        SupplierPartyType supplier = invoice.getAccountingSupplierParty();
        checkId("CO001", supplier.getCustomerAssignedAccountID());
        checkParty(supplier.getParty(), "Cenvet Pty Ltd", "26", "Binney Road", "Kings Park", "2148", "NSW");

        // check amounts
        checkTaxTotal("10.00", invoice.getTaxTotal());

        MonetaryTotalType legalMonetaryTotal = invoice.getLegalMonetaryTotal();
        checkAmount("110.00", legalMonetaryTotal.getPayableAmount());
        checkAmount("100.00", legalMonetaryTotal.getLineExtensionAmount());

        // check the invoice line
        assertEquals(1, invoice.getInvoiceLine().size());
        InvoiceLineType line = invoice.getInvoiceLine().get(0);
        checkId("A", line.getID());
        checkQuantity(line.getInvoicedQuantity(), "BO", 2);
        checkAmount("100.00", line.getLineExtensionAmount());

        OrderLineReferenceType orderRef = line.getOrderLineReference().get(0);
        checkId("20123", orderRef.getLineID());
        checkTaxTotal("10.00", line.getTaxTotal());

        checkItem(line.getItem(), "Amoxil 200mg Tablets", "Bottle Amoxil 200mg Tablets", "123456", "AMOX200100");

        checkAmount("50.00", line.getPrice().getPriceAmount());
    }


}